<?php

    // define('DB_HOST','localhost');
    // define('DB_NAME','popovxyz_hakaton2019');
    // define('DB_USER','popovxyz_hakaton');
    // define('DB_PASS','hakaton2019');
    define('DB_HOST','localhost');
    define('DB_NAME','brainste_team4');
    define('DB_USER','brainste_team4');
    define('DB_PASS','Team4Hackaton');
    
    define('DB_INFO','mysql:host='.DB_HOST.';dbname='.DB_NAME);

    //**********************************************************************************************
    try {
        $db=new PDO(DB_INFO,DB_USER,DB_PASS);   // kreiranje konekcija do bazata preku constanti
        // echo "db connected";                    // proverka dali e vospostavena konekcija
        //$db=null;                               // dokolku se e vo red, ja zatvara konecijata
    } catch (PDOException $e){                  
        echo "ERROR: ".$e->getMessage();          // dokolku ima nekoj problem, da go ispise
        die();
    }
    //**********************************************************************************************