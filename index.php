<?php
    include_once 'db.php';
    session_start();


?>
<!DOCTYPE html>
<html>

<head>
    <title>Chat</title>
    <meta name='keywords' content='html,css,lekcii'>
    <meta name='description' content='ova e strana za ucenje'>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width,initial-scale=1.0'>
    <!-- Latest compiled and minified BOOTSTRAP CSS -->
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <!-- Local CSS -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap" rel="stylesheet">


    <link rel='stylesheet' type='text/css' href='style.css'>
    
    <!-- Font-awesome 4.7 cdn -->
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
</head>

<body>

    <nav id="main" class="nav navbar-fixed-top">
        <div class="container shadow-container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#home">
                    <img class="logo" src="logo.png">                        
                    </a>
                    <a href="" class="search-button"><i class="fa fa-search search " aria-hidden="true"></i></a>
                    <input type="text" class="search-input">
                </div>
                <div class="input-field">
                    <ul class="nav navbar-nav navbar-right">
                        <form action="" method="post">
                            <div class='form-group'>
                                <span><i class="fa fa-envelope-o"></i></span>
                                <input type="text" id="input" name="subscribe" placeholder="Get two new looks every week">
                                <button type="submit" name="mail_submit" value="mail_submit" class="btn btn-default findBtn"><b>Find out first</b>
                            </div>
                        </form>
                    </ul>
                </div>
                <?php
                    if($_SERVER['REQUEST_METHOD']=="POST" && isset($_POST['mail_submit'])){
                        $sql='INSERT INTO `newsletter`(`subscribe-mail`) VALUES (:email)';
                        $sth=$db->prepare($sql);
                        $sth->bindParam(`:email`,$_POST['subscribe']);
                        $sth->execute();
                    }
                ?>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row margin-top-nav">
            <div class="col-md-3 sidebar">
                <ul class="sidebar-list">

                    <!-- generiranje na meni items od tabela category -->
                    <?php
                        $sql='SELECT * FROM category';
                        $sth=$db->prepare($sql);
                        $sth->execute();
                        $results=$sth->fetchAll();
                        // $categoryNo=count($results);
                        for($i=0;$i<count($results);$i++){
                            if($results[$i]['category']=='ADD NEW COMPANY'){
                                // Koga ke dojde do ADD NEW COMPANY da ne ja prikazuva
                                // da i dodade class hide
                                echo '<li class="hide"><a href="#">';
                            } elseif ($results[$i]['category']=='HIRING') {
                                echo '<li><a href="#" class="hirring '.$results[$i]['className'].'">';
                            } else {
                                echo '<li><a href="#" class="'.$results[$i]['className'].'">';
                            }
                            echo $results[$i]['category'].'</a></li>';
                        }
                    ?>
                </ul>

                <div class="sidebar-icons">
                    <i class="fa fa-linkedin sidebar-icons-in" aria-hidden="true"></i>
                    <i class="fa fa-twitter sidebar-icons-twiter" aria-hidden="true"></i>
                </div>
            </div>
            <!-- -------------------FORM------------------------- -->


            <div class="col-md-9 col-md-offset-3 formOnClick">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <form action="" method="post">
                            <span>Add your company here</span>
                            <div class="form-group">
                                <input type="text" class="form-control bgc" name="companyName" id="CompanyName" placeholder="Company name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control bgc" name="companyWebSite" id="CompanyWebSite"
                                    placeholder="Company website">
                            </div>
                            <div>
                                <textarea class="form-control bgc" rows="4" name="about" placeholder="About the company"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="Radio">Do you work for this company?</label><br>
                                <input class="btn btn-default bgc bgc-btn" type="button" name="working" id="Radio"
                                    value="Yes">
                                <input class="btn btn-default bgc bgc-btn" type="button" name="working" id="Radio"
                                    value="No">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Your email address</label>
                                <input type="email" class="form-control bgc" name="email" id="emailAddress"
                                    placeholder="example@example.com">
                            </div>

                            <button type="submit" class="btn btn-default bgc bgc-btn" name="submit" value="addCompany"><b>Submit</b></button>
                        </form>
                    </div>
                </div>
            </div>



            <div class="col-md-9 padding col-md-offset-3 hidde">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bg-image1">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row first">
                            <div class="col-md-12 second">
                                <h3 class="bold">Lorem Ipsum</h3>
                            </div>
                            <div class="col-md-12">
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cupiditate laudantium
                                    voluptate sapiente velit laborum unde, libero blanditiis odio quis optio sunt
                                    soluta eius consectetur porro ex voluptatibus aliquid quaerat repellat.<p>
                                        <div>
                                        <button class="btn lightblue">Find out more</button> 
                                        <i class="fa fa-share-alt share-main" aria-hidden="true" style="color: #cdcdcd"></i>

                                        </div>
                                       

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row padding">
                <!-- Generiranje na kartickite od baza  -->
                    <?php
                        $sql='SELECT * FROM cards JOIN category ON cards.category_id=category.categoryID ORDER BY views DESC';
                        $sth=$db->prepare($sql);
                        $sth->execute();
                        $cards=$sth->fetchAll();

                        for($i=0;$i<count($cards);$i++){
                            
                            echo '<div class="col-md-4 card '.$cards[$i]['className'].'"  data-toggle="modal" data-target="#myModal" onclick="myAjax('.$cards[$i]['id'].', '.$cards[$i]['views'].')">
                                    <div class="bg-image" style="background-image: url('.$cards[$i]['cover_img'].'">
                                        <div class="overlay">
                                            <div class="row padding-top">
                                                <div class="col-md-10 col-md-offset-1 positions-info">
                                                    <p class="bigger-font">'.$cards[$i]['header'].'</p><br>
                                                    <p class="text-left small-font">'.$cards[$i]['category'].'</p>
                                                    <p class="views" hidden id="'.$cards[$i]['id'].'" >'.$cards[$i]['views'].'</p>
                                                    <div class="crta" style="background-color:'.$cards[$i]['category_color'].';"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                        }
                        $sth->closeCursor();
                         //added views and card id to every card in hidden paragraph
                    ?>
                </div>
            </div>
        </div>
    </div>  <!-- end of container -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog  myModal" role="document">
                        <div class="row  modal-content">
                            <div class="col-md-4 modal-sideBar">
                                <div class="modal-sidebar-title">
                                    <h2>Lorem Ipsum is simply dummy text of the printing and type industry</h2>
                                </div>


                                <div class="category">
                                    <h5>HE EDUCATION</h5>
                                    <div class="crta"></div>
                                </div>

                            </div>
                            <div class="col-md-8 modal-body-content">
                                <div class="main-img">
                                    <button type="button" class="close close-button" data-dismiss="modal"
                                        aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                </div>
                                <div class="modal-inner">
                                    <div class="row modal-about">
                                        <div class="col-md-6">
                                            <h4><b> What is Lorem Ipsum?</b></h4>
                                        </div>
                                        <div class="col-md-6">
                                            <h6> <b> Rregular for the text Bold for the title</b></h6>
                                        </div>

                                    </div>
                                    <div>
                                        <i class="fa fa-share-alt" aria-hidden="true" style="color: #cdcdcd"></i>
                                        <i class="fa fa-linkedin" aria-hidden="true" style="color: #cdcdcd"></i>

                                    </div>
                                    <div class="modal-text-boxes">
                                        <span class="modal-text-content"><b>The standart Lorem Ipsum passage, used since
                                                the 1500s</b> </span>
                                        <p class="modal-text-content">"Lorem ipsum dolor sit, amet consectetur
                                            adipisicing elit. Omnis quam ducimus
                                            quaerat quisquam doloribus quibusdam voluptates amet quod id. Aliquam
                                            eveniet
                                            qui debitis aperiam ab similique mollitia cumque modi sunt?"</p>
                                    </div>
                                    <div class="modal-text-boxes">
                                        <span class="modal-text-content"><b>Section 1.10.32 of "de FiniBus Bonorun et
                                                Malorum",Writen by Cicero in
                                                45bc</b> </span>
                                        <p class="modal-text-content">"Lorem ipsum dolor sit amet, consectetur
                                            adipisicing elit. Recusandae
                                            consequuntur
                                            magni doloribus unde minus, aliquam ullam error a, ipsam, doloremque impedit
                                            nostrum quia? Dolore, error est! Nam officiis quasi rem. Lorem ipsum dolor
                                            sit
                                            amet consectetur adipisicing elit. Earum numquam ipsam amet error a nulla ad
                                            veritatis voluptates ratione, accusantium sapiente voluptatibus recusandae
                                            magni
                                            voluptate voluptatum dolor similique possimus iste."</p>
                                    </div>
                                    <div class="attract-talent">
                                        <img src="Hakaton Design/sliki/How to attract talent/Become an expert in creating experts.jpg"
                                            class="img img-responsive" alt="">

                                    </div>
                                    <div class="modal-text-boxes">
                                        <span class="modal-text-content"><b>1914 translation by H. Rockham</b></span>
                                        <p class="modal-text-content">"Lorem ipsum dolor sit amet, consectetur
                                            adipisicing elit. Nemo necessitatibus
                                            hic
                                            eius porro repellendus, mollitia saepe ducimus quod corporis, natus quidem
                                            dolorem doloribus velit tempore odit dolore similique cupiditate
                                            perspiciatis.
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum at
                                            nesciunt! Consequuntur, nemo voluptatum illum vero deserunt, odio cum esse
                                            expedita harum iste iure alias corporis ut. Totam, repellendus." </p>
                                        <br><br>
                                        <p class="modal-text-content">"Lorem ipsum dolor sit amet, consectetur
                                            adipisicing elit. Nemo necessitatibus
                                            hic
                                            eius porro repellendus, mollitia saepe ducimus quod corporis, natus quidem
                                            dolorem doloribus velit tempore odit dolore similique cupiditate
                                            perspiciatis.
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum at
                                            nesciunt! Consequuntur, nemo voluptatum illum vero deserunt, odio cum esse
                                            expedita harum iste iure alias corporis ut. Totam, repellendus."</p><br><br>

                                    </div>
                                    <div>
                                        <form action="" method="POST">
                                            <div class="letter-change">
                                                <span><i class="fa fa-envelope-o"></i></span>
                                                <input type="text" class="modal-input"
                                                    placeholder="example@example.com">
                                                <button class="modal-button" name="mail_submit" value="mail_submit"><b>Stay Updated</b></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- jQuery library -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js'></script>
<!-- Latest compiled JavaScript -->
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script src="main.js">


// $('li a').on('click', function(){
//   $('.card.'+$(this).attr('class')).toggle();
// });

// })

</script>
<script src="ajax.js"></script>

</body>

</html>
<?php
    if($_SERVER['REQUEST_METHOD']=='POST' && $_POST['submit']=='addCompany'){
        $sql='INSERT INTO temp_company (`name`, `web_site`, `about`, `working`, `email`)
                VALUES (:name, :web, :about, :working, :mail);';
        $sth=$db->prepare($sql);
        $sth->bindParam(`:name`, $_POST['companyName']);
        $sth->bindParam(`:web`, $_POST['companyWebSite']);
        $sth->bindParam(`:about`, $_POST['about']);
        $sth->bindParam(`:working`, $_POST['working']);
        $sth->bindParam(`:mail`, $_POST['email']);

        $sth->execute();
    }
    ?>